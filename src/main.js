import Vue from 'vue'
import App from './App.vue'
import VueApexCharts from 'vue-apexcharts'
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import router from './router'
import Highcharts from 'highcharts';
import HighchartsVue from 'highcharts-vue';
import mapInit from 'highcharts/modules/map';
import world from '@highcharts/map-collection/custom/world.geo';

Vue.config.productionTip = false;

Vue.component('apexchart', VueApexCharts);
Vue.component('v-select', vSelect);

mapInit(Highcharts);

Highcharts.maps["custom/world"] = world;

Vue.use(HighchartsVue);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
