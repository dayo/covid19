import Vue from 'vue'
import VueRouter from 'vue-router';
import Single from "./views/Single";
import Global from "./views/Global";
import Multiple from "./views/Multiple";
import Loading from "./views/Loading";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    base: process.env.NODE_ENV === 'production' ? '/covid19/' : '/',
    routes: [
        {path: '/', component: Loading},
        {path: '/single', component: Single},
        {path: '/multiple', component: Multiple},
        {path: '/global', component: Global},
    ],
});

export default router;
